import statsmodels.api as sm
from scipy.stats import laplace
import numpy as np
import matplotlib.pyplot as plt
import math

# standard deviation of white noise part of time series X_t
sigma = 0.5

# define the model via module statsmodels

arparams = np.array([-0.2,-0.9])
maparams = np.array([0,1])
ar = np.r_[1, -arparams]
ma = np.r_[1, maparams]
arma_process = sm.tsa.ArmaProcess(ar, ma)
gamma_X = arma_process.acovf(500)
nu = gamma_X[0] + sigma**2

def f(x):
	n_coeffs = len(gamma_X)
	res = nu
	for k in range(1,n_coeffs):
		res += 2 * gamma_X[k] * np.cos(k * x)
	return(res/(2*np.pi))
	
t = np.arange(-np.pi, np.pi, 0.02)
Y = np.array([f(x) for x in t])

def do_one(x):
	n = x[0]
	alpha = x[1]
	kappa = x[2]
	T_local = x[3]
	call_sign = x[4]
	X1 = arma_process.generate_sample(n)
	X2 = np.random.normal(0, sigma, n)
	X = X1 + X2
	
	nuhat = nu
	#tau_n = np.sqrt(56 * nuhat * np.log(n))
	#tau_n = 4
	tau_n = np.log(n)/np.log(10)
	Xtilde = np.minimum(np.maximum(X,-tau_n),tau_n)
	
	noise = laplace.rvs(scale=2*tau_n/alpha,size=n)
	
	Z = Xtilde + noise
	
	# initialization
	c = np.zeros(n)

	# mean of privatized data
	Z_mean = np.mean(Z)

	# compute empirical covariances
	# nullter koeffizient stimmt noch nicht wegen privacy
	for r in np.arange(n):
		for k in np.arange(0,n-r):
			c[r] += 1/n * (Z[k] - Z_mean) * (Z[k+r] - Z_mean)

	c[0] = c[0] - 8 * tau_n**2 / alpha**2
	
	d_max = 50
	A = np.zeros((d_max,d_max))
	for i in np.arange(d_max): # model that corresponds to d = i+1
		for j in np.arange(i+1): # index over coefficients for this model
			# for the following formula, see last display on p. 279 in Comte (2001)
			A[i,j] = (c[0])/(2*(i+1))
			for r in np.arange(1,n):
				A[i,j] += 1/np.pi * c[r]/r * (np.sin(np.pi*(j+1)*r/(i+1)) - np.sin(np.pi * j * r/(i+1)))
			A[i,j] = A[i,j] * np.sqrt((i+1)/np.pi)
	
	# auxiliary estimator sup norm of f
	#d_ref = 2
	C_infty_d_ref = 1 #np.amax(np.absolute(A[d_ref - 1,:]))
	
	# initialize array for values of penalized contrast criterion
	pen_contrast = np.zeros(d_max)
	
	# compute penalized contrasts; cf. formula (19) in Comte (2001)
	for i in np.arange(d_max):
		pen_contrast[i] = -np.sum(np.square(A[i,:])) + kappa * C_infty_d_ref**2 * (i+1)/n * max(1,tau_n**4/alpha**4)
		
	# model selection
	i_hat = np.argmin(pen_contrast)
	d_adaptive = i_hat + 1

	# adaptive estimator
	def fhat(x):
		# look for index of the (uniquely determined) basis function with x in its support
		j = math.floor(abs(x) * d_adaptive/np.pi)
		if j == d_max: # x = pi
			j = d_max - 1
		# return the appropriately scaled coefficient as the function value
		return(np.sqrt(d_adaptive/np.pi) * A[i_hat,j])
	
	#Yhat = np.array([max(0,fhat(x)) for x in t])
	Yhat = [fhat(x) for x in t]
	error = np.mean(np.square(Yhat - Y))

	# plot
	plt.plot(t, Y, 'b:')
	plt.plot(t, Yhat, 'r-')
	path_to_plot = call_sign + '/plots/n_' + str(n) + '_alpha_' + str(alpha) + '_kappa_' + str(kappa) + '_T_' + str(T_local) + '.pdf'
	plt.savefig(path_to_plot)
	plt.close()
	
	output = np.hstack((np.array([n,alpha,kappa,error]),Yhat))
	
	return(output)


	
	
	
	
	
	
	

