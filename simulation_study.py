from do_one_simulation import *
import numpy as np
import random
import os
import sys
import multiprocessing as mp

file = open('config.txt','r')
lines = [line for line in file]

n_list = np.array(lines[1].split(),dtype='int')

alpha_list = np.array(lines[3].split(),dtype='float')
alpha_list = np.insert(alpha_list, 0, np.inf)

kappa_list = np.array(lines[5].split(),dtype='float')
# eigentliche simulationsstudie
#for x, y in [(x,y) for x in a for y in b]:

T = int(lines[7])

my_seed = int(lines[9])

call_sign = lines[11]
assert not os.path.isdir(call_sign); 'call_sign has already been used! Either create new call_sign or delete old data'
os.mkdir(call_sign)
os.mkdir(call_sign + '/plots')

copy_config_cmd = 'cp config.txt ' + call_sign + '/config.txt'
os.system(copy_config_cmd)

copy_do_one_cmd = 'cp do_one_simulation.py ' + call_sign + '/do_one_simulation.py'
os.system(copy_do_one_cmd)

copy_study_cmd = 'cp simulation_study.py ' + call_sign + '/simulation_study.py'
os.system(copy_study_cmd)

job_list = [[n,alpha,kappa,i,call_sign] for n in n_list for alpha in alpha_list for kappa in kappa_list for i in range(T)]

# initialize 
results = np.empty([len(job_list), len(np.arange(-np.pi, np.pi, 0.02)) + 4])

# set seeds before starting simulations
np.random.seed(my_seed)
random.seed(my_seed)

pool = mp.Pool(mp.cpu_count())
results = pool.map(do_one, [row for row in job_list])
pool.close()

#for counter, x in enumerate(job_list):
#	result_this_simulation = do_one(x)
#	results[counter,:] = result_this_simulation

np.savetxt(call_sign + '/results.csv',results,delimiter=',')

print('Done (' + call_sign + ')!')
