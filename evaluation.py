import numpy as np
import statsmodels.api as sm
import matplotlib.pyplot as plt
import sys
import os

call_sign = 'simulation2'

assert os.path.isdir(call_sign); 'there is no simulation for this call_sign'

# read parameter configurations of this simulation
file = open(call_sign + '/config.txt','r')
lines = [line for line in file]

n_list = np.array(lines[1].split(),dtype='int')

alpha_list = np.array(lines[3].split(),dtype='float')
alpha_list = np.insert(alpha_list, 0, np.inf)

kappa_list = np.array(lines[5].split(),dtype='float')

T = int(lines[7])
print(T)

# load data
data = np.genfromtxt(call_sign + '/results.csv',delimiter=',')

# compute mean errors and sds

n_and_alpha_list = [(n,alpha) for n in n_list for alpha in alpha_list]

# initialize array that contains the values for tables in paper
mean_errors_and_cis = np.zeros((len(n_and_alpha_list),4))

print(mean_errors_and_cis)
 
for counter, (n, alpha) in enumerate(n_and_alpha_list):
	mean_errors_and_cis[counter,0] = n
	mean_errors_and_cis[counter,1] = alpha
	
	indices_n = np.where(data[:,0] == n)
	indices_alpha = np.where(data[:,1] == alpha)
	ind = np.intersect1d(indices_alpha,indices_n)
	
	mean_errors_and_cis[counter,2] = np.mean(data[ind,3])
	
	mean_errors_and_cis[counter,3] = 1.96 * np.std(data[ind,3])/np.sqrt(T)
 
print(np.round(mean_errors_and_cis,decimals=4))
 
# now produce some plots
 
# grid on horizontal axis 
t = np.arange(-np.pi, np.pi, 0.02)

# plot for true spectral density

# standard deviation of white noise part of time series X_t
sigma = 0.5

# define the model via module statsmodels

arparams = np.array([-0.2,-0.9])
maparams = np.array([0,1])
ar = np.r_[1, -arparams]
ma = np.r_[1, maparams]
arma_process = sm.tsa.ArmaProcess(ar, ma)
gamma_X = arma_process.acovf(500)
nu = gamma_X[0] + sigma**2

def f(x):
	n_coeffs = len(gamma_X)
	res = nu
	for k in range(1,n_coeffs):
		res += 2 * gamma_X[k] * np.cos(k * x)
	return(res/(2*np.pi))
	
Y = np.array([f(x) for x in t])
 
# n = 10000

n = 1000
indices_n = np.where(data[:,0] == n)

for counter, alpha in enumerate(alpha_list):
	indices_alpha = np.where(data[:,1] == alpha)
	ind = np.intersect1d(indices_alpha,indices_n)
	aux_data = data[ind,:]
	mean_data = np.mean(aux_data,axis=0)
	upper_quantile_data = np.quantile(aux_data,0.95,axis=0)
	lower_quantile_data = np.quantile(aux_data,0.05,axis=0)
	if alpha == np.inf:
		plt.plot(t,Y,'k--',linewidth=1)
		plt.plot(t,mean_data[4:],'r-',linewidth=1.25, alpha=0.85)
		plt.plot(t,upper_quantile_data[4:],'r:')
		plt.plot(t,lower_quantile_data[4:],'r:')
		plt.savefig('meanplot_n_' + str(n) + '_alpha_' + str(alpha)  + '.pdf')
		plt.close()
	elif alpha == 5:
		plt.plot(t,Y,'k--',linewidth=1)
		plt.plot(t,mean_data[4:],'r-',linewidth=1.25, alpha=0.85)
		plt.plot(t,upper_quantile_data[4:],'r:')
		plt.plot(t,lower_quantile_data[4:],'r:')
		plt.savefig('meanplot_n_' + str(n) + '_alpha_' + str(alpha).replace('.','_')  + '.pdf')
		plt.close()
	elif alpha == 2.5:
		plt.plot(t,Y,'k--',linewidth=1)
		plt.plot(t,mean_data[4:],'r-',linewidth=1.25, alpha=0.85)
		plt.plot(t,upper_quantile_data[4:],'r:')
		plt.plot(t,lower_quantile_data[4:],'r:')
		plt.savefig('meanplot_n_' + str(n) + '_alpha_' + str(alpha).replace('.','_')  + '.pdf')
		plt.close()

# n= 20000

n = 2000
indices_n = np.where(data[:,0] == n)

for counter, alpha in enumerate(alpha_list):
	indices_alpha = np.where(data[:,1] == alpha)
	ind = np.intersect1d(indices_alpha,indices_n)
	aux_data = data[ind,:]
	mean_data = np.mean(aux_data,axis=0)
	upper_quantile_data = np.quantile(aux_data,0.95,axis=0)
	lower_quantile_data = np.quantile(aux_data,0.05,axis=0)
	if alpha == np.inf:
		plt.plot(t,Y,'k--',linewidth=1)
		plt.plot(t,mean_data[4:],'r-',linewidth=1.25, alpha=0.85)
		plt.plot(t,upper_quantile_data[4:],'r:')
		plt.plot(t,lower_quantile_data[4:],'r:')
		plt.savefig('meanplot_n_' + str(n) + '_alpha_' + str(alpha)  + '.pdf')
		plt.close()
	elif alpha == 5:
		plt.plot(t,Y,'k--',linewidth=1)
		plt.plot(t,mean_data[4:],'r-',linewidth=1.25, alpha=0.85)
		plt.plot(t,upper_quantile_data[4:],'r:')
		plt.plot(t,lower_quantile_data[4:],'r:')
		plt.savefig('meanplot_n_' + str(n) + '_alpha_' + str(alpha).replace('.','_')  + '.pdf')
		plt.close()
	elif alpha == 2.5:
		plt.plot(t,Y,'k--',linewidth=1)
		plt.plot(t,mean_data[4:],'r-',linewidth=1.25, alpha=0.85)
		plt.plot(t,upper_quantile_data[4:],'r:')
		plt.plot(t,lower_quantile_data[4:],'r:')
		plt.savefig('meanplot_n_' + str(n) + '_alpha_' + str(alpha).replace('.','_')  + '.pdf')
		plt.close()
